using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeviceOnEnableEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent onEnablePc = null;
    [SerializeField] private UnityEvent onEnableMobile = null;

    private void OnEnable()
    {
        if (Device.IsMobileOrTablet)
            onEnableMobile?.Invoke();
        else
            onEnablePc?.Invoke();
    }
}
