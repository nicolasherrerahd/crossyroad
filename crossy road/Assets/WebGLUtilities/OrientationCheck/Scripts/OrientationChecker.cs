using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationChecker : MonoBehaviour
{
    [SerializeField] private RectTransform canvas = null;
    [SerializeField] private GameObject blockingPanel = null;


    private bool IsLandscape
    {
        get => isLandscape;
        set
        {
            if (isLandscape == value) return;
            isLandscape = value;
            HandleOrientationChanged();
        }
    }


    private void Start()
    {
        lastTimeScale = Time.timeScale;
    }

    private void HandleOrientationChanged()
    {
        blockingPanel.SetActive(!IsLandscape);
        if (!IsLandscape)
            lastTimeScale = Time.timeScale;
        Time.timeScale = IsLandscape ? lastTimeScale : 0;

    }

    private bool isLandscape;

    private float lastTimeScale;

    private void Update()
    {
        IsLandscape = canvas.rect.width > canvas.rect.height;
    }

}
