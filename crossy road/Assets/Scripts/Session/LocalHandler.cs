using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LocalHandler : MonoBehaviour
{
    [SerializeField] private UnityEvent onLocal = null;

    private void Start()
    {
        if (!Local.IsLocal) return;
        onLocal?.Invoke();
    }
}
