using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SessionData
{
    public static int ConfigId { get; private set; }
    public static int GameId { get; private set; }
    public static int LessonId { get; private set; }

    public static void Set(JSONNode json)
    {
        ConfigId = json["configid"];
        GameId = json["gameid"];
        LessonId = json["lessonid"];
        Debug.Log($"Data set successfully: {ConfigId}");
    }
}
