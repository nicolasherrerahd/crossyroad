﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageInfo
{
    public int Score { get; set; }
    public int Attempts { get; set; }
    public bool Completed { get; set; }
    public List<QuestionData> Questions { get; } = new List<QuestionData>();

    public StageInfo(StageStats stats, List<QuestionData> questions)
    {
        Score = stats.points;
        Attempts = stats.attempts;
        Completed = stats.completed;
        foreach (var question in stats.listQuestion)
            Questions.Add(questions.Find(q => q.id == question.id));
    }

    public StageInfo()
    {
        Score = Attempts = 0;
        Completed = false;
    }

    public int SetMaxScore(int score)
    {
        if (score > Score)
            Score = score;
        return Score;
    }
}
