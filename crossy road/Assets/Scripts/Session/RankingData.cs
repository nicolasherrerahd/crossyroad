using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingData
{
    public int Id { get; set; }
    public int Position { get; set; }
    public int Score { get; set; }
    public string Name { get; set; }

    public RankingData()
    {
            
    }

    public RankingData(JSONNode item, int index)
    {
        Id = item["userid"];
        Score = item["points"];
        Position = index + 1;
    }

    public override string ToString()
    {
        return $"Id: {Id}\nName: {Name}\nScore: {Score}";
    }

    public static int[] GetListIds(RankingData[] data)
    {
        int[] listIds = new int[data.Length];
        for (int i = 0; i < data.Length; i++)
        {
            listIds[i] = data[i].Id;
        }
        return listIds;
    }
}
