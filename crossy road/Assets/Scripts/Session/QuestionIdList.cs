using SimpleJSON;
using System.Collections.Generic;

[System.Serializable]
public class QuestionIdList
{
    public long[] listIds;

    public QuestionIdList(int length)
    {
        listIds = new long[length];
    }

    public QuestionIdList(int resourceTypeId, JSONNode questionsData)
    {
        var ids = new List<long>();
        foreach (var question in questionsData["listGame_config_x_resources"])
        {
            //UnityEngine.Debug.Log($"Resource type Id: {question.Value["resource_type_id"]["id"]}\nResource Id: {question.Value["resource_id"]}");
            if (question.Value["resource_type_id"]["id"] == resourceTypeId)
            {
                ids.Add(question.Value["resource_id"]);
            }
        }
        listIds = ids.ToArray();
    }
}
