﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Category
{
    public List<QuestionData> QuestionsData { get; } = new List<QuestionData>();
    public List<QuestionData> CorrectQuestions { get; } = new List<QuestionData>();
    public CategoryData Data { get; private set; }
    public List<StageInfo> Stages { get; } = new List<StageInfo>();
    public StageInfo CurrentStage { get; set; }
    public List<QuestionData> CurrentStageQuestions => CurrentStage.Completed ? CurrentStage.Questions : GetRemainingQuestions();
    public int Score { get; set; }
    public int AccumulatedScore { get; set; }
    public int CompletedStagesCount { get; set; }
    public int HighestStageScore { get; set; }
    public int TotalAttempts { get; set; }

    public long EnrollmentGroupId { get; set; }

    public bool Completed { get; set; }

    private List<QuestionData> GetRemainingQuestions()
    {
        var remainingQuestions = new List<QuestionData>(QuestionsData);
        foreach (var stage in Stages)
        {
            foreach (var question in stage.Questions)
            {
                var repeated = remainingQuestions.Find(q => q.id == question.id);
                remainingQuestions.Remove(repeated);
            }
        }
        return remainingQuestions;
    }

    public void AddCorrectQuestion(QuestionData currentQuestion)
    {
        var question = QuestionsData.Find(q => q.id == currentQuestion.id);
        QuestionsData.Remove(question);
        CorrectQuestions.Add(question);
        if (QuestionsData.Count < Data.questionsByStage)
        {
            QuestionsData.AddRange(CorrectQuestions);
            CorrectQuestions.Clear();
        }
    }

    public void LoadStats(CategoryStats stats)
    {
        if (stats == null) return;
        Score = stats.points;
        Completed = stats.completed;
        CompletedStagesCount = stats.stagesOK;
        HighestStageScore = stats.maxStage;
        TotalAttempts = stats.totAttempts;
        AccumulatedScore = stats.totPoints;
        foreach (var stageStats in stats.listStages)
            Stages.Add(new StageInfo(stageStats, QuestionsData));
    }

    public void AddAttempt(int attemptScore)
    {
        CurrentStage.Attempts++;
        AccumulatedScore += attemptScore;
        if (CurrentStage.SetMaxScore(attemptScore) > HighestStageScore)
            HighestStageScore = attemptScore;
    }

    public void MarkCurrentStageComplete(IEnumerable<QuestionData> usedQuestions)
    {
        if (CurrentStage.Completed) return;
        CompletedStagesCount++;
        CurrentStage.Completed = true;
        CurrentStage.Questions.AddRange(usedQuestions);
    }

    public Category(CategoryData data)
    {
        QuestionsData.Clear();
        Completed = false;
        Data = data;
    }
}

[System.Serializable]
public class CategoryData
{
    public long id;
    public string name;
    public string description;
    public string time;
    public int stages;
    public int questionsByStage;
    public int attempts;
    public long triviaId;

    public int GetTimeValue()
    {
        string[] timeValues = time.Split(':');
        int hours = int.Parse(timeValues[0]);
        int minutes = int.Parse(timeValues[1]);
        int seconds = int.Parse(timeValues[2]);
        return seconds + minutes * 60 + hours * 3600;
    }
}