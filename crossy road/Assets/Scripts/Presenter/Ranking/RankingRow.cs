using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingRow : MonoBehaviour
{
    [SerializeField] private Text positionText = null;
    [SerializeField] private Text usernameText = null;
    [SerializeField] private Text scoreText = null;
    [SerializeField] private VarReplacer scoreReplacer = new VarReplacer("{s} pts");
    [SerializeField] private Highlighter highlighter = null;

    public int Id { get; private set; }

    public void Set(RankingData data)
    {
        gameObject.SetActive(true);
        positionText.text = data.Position.ToString();
        usernameText.text = data.Name;
        scoreText.text = scoreReplacer.GetReplacedText(data.Score.ToString());
        Id = data.Id;
        if (highlighter != null)
        {
            highlighter.Highlight(Id == User.Id);
        }
    }

    public void Restart(string defaultName, int index)
    {
        positionText.text = $"{index + 1}";
        usernameText.text = defaultName;
        scoreText.text = scoreReplacer.GetReplacedText(0);
    }

}
