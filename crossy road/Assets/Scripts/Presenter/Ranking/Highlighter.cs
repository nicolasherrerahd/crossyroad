using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highlighter : MonoBehaviour
{
    [SerializeField] private GameObject background = null;
    [SerializeField] private Text[] texts = null;
    [SerializeField] private Color textColor = Color.white;

    private Color originalColor;

    private void Awake()
    {
        originalColor = texts.Length > 0 ? texts[0].color : Color.white;
    }

    public void Highlight(bool enabled)
    {
        background.SetActive(enabled);
        foreach (var text in texts)
        {
            text.color = enabled ? textColor : originalColor;
        }
    }

}
