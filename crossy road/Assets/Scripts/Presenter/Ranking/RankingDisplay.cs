using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingDisplay : MonoBehaviour
{
    [SerializeField] private RankingLoader loader = null;
    [SerializeField] private GameObject panel;
    [SerializeField] private List<RankingRow> rankingRows = new List<RankingRow>();
    [SerializeField] private string defaultName = "Sin Nombre";
    [SerializeField] private RectTransform content = null;
    [SerializeField] private ScrollAimer aimer = null;

    private void Awake()
    {
        loader.OnLoaded += UpdateRanking;
    }

    private void UpdateRanking()
    {
        panel.SetActive(true);
        CheckRowsNeeded();
        UpdateRows();
    }
    private void UpdateRows()
    {
        var rowsData = loader.RowsData;
        for (int i = 0; i < rowsData.Length; i++)
            rankingRows[i].Set(rowsData[i]);
        var customRow = rankingRows.Find(r => r.Id == User.Id);
        if (customRow != null)
            aimer.AimAt(customRow.transform);
    }

    private void CheckRowsNeeded()
    {
        if (loader.FullTop > rankingRows.Count)
            AddRows(loader.FullTop - rankingRows.Count);
        int diff = loader.RowsData.Length - rankingRows.Count;
        if (diff > 0)
            AddRows(diff);
        else if (diff < 0)
            DisableRemainingRows(diff);
    }

    private void AddRows(int diff)
    {
        for (int i = 0; i < diff; i++)
            rankingRows.Add(Instantiate(rankingRows.GetLastItem(), content));
    }

    private void DisableRemainingRows(int diff)
    {
        for (int i = rankingRows.Count + diff; i < loader.FullTop; i++)
            rankingRows[i].Restart(defaultName, i);
        for (int i = loader.FullTop; i < rankingRows.Count; i++)
        {
            rankingRows[i].gameObject.SetActive(false);
        }
    }
}
