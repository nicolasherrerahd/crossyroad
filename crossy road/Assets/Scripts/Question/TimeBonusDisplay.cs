using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrossyRoad
{
    public class TimeBonusDisplay : MonoBehaviour
    {
        [SerializeField] private TimeBonusHandler handler = null;
        [SerializeField] private Timer timer = null;
        [SerializeField] private Text text = null;
        [SerializeField] private Color maxBonusColor = Color.white;
        [SerializeField] private Color decreasingBonusColor = Color.yellow;
        [SerializeField] private Color emptyBonusColor = Color.red;

        private void Awake()
        {
            timer.OnTimeUpdated += HandleTimeUpdated;
        }

        private void OnDestroy()
        {
            timer.OnTimeUpdated -= HandleTimeUpdated;
        }

        private void HandleTimeUpdated(int currentTime)
        {
            Color color = Color.white;
            switch (handler.State)
            {
                case BonusState.Max:
                    color = maxBonusColor;
                    break;
                case BonusState.Decreasing:
                    color = decreasingBonusColor;
                    break;
                case BonusState.Empty:
                    color = emptyBonusColor;
                    break;
            }
            text.color = color;
        }

    }

}
