﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultipleChoiceDisplay : MonoBehaviour
{
    [SerializeField] private Text multipleChoiceText = null;
    [SerializeField] private string correctOptsWildcard = "{r}";

    private string originalText;

    private void Awake()
    {
        StageHandler.OnQuestionUpdated += HandleQuestionUpdated;
        originalText = multipleChoiceText.text;
    }
    private void OnDestroy()
    {
        StageHandler.OnQuestionUpdated -= HandleQuestionUpdated;
    }

    private void HandleQuestionUpdated(QuestionData question)
    {
        multipleChoiceText.text = question.multiresponse
            ? $"{originalText.Replace(correctOptsWildcard, question.GetCorrectOptions().Length.ToString())}"
            : string.Empty;
    }
}
