using CrossyRoad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreakDisplay : MonoBehaviour
{
    [SerializeField] private Text text = null;
    [SerializeField] private VarReplacer replacer = new VarReplacer("{s}/{t}");

    private void Awake()
    {
        StageSummary.OnSummaryDataUpdated += HandleSummaryDataUpdated;
    }

    private void OnDestroy()
    {
        StageSummary.OnSummaryDataUpdated -= HandleSummaryDataUpdated;
    }


    private void HandleSummaryDataUpdated(StageSummary summary)
    {
        text.text = replacer.GetReplacedText(new object[] { summary.CorrectQuestionsCount, summary.QuestionsCount });
    }
}
