﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryProgressDisplay : MonoBehaviour
{
    [SerializeField] private Text scoreText = null;
    [SerializeField] private string pointsCode = "{cp}";
    [SerializeField] private string completedStagesCountCode = "{csc}";
    [SerializeField] private string totalStagesCountCode = "{tsc}";

    private string originalPtsText;

    private void Awake()
    {
        originalPtsText = scoreText.text;
    }

    public void Set()
    {
        scoreText.text = originalPtsText.Replace(pointsCode, User.CurrentCategory.Score.ToString());
        scoreText.text = scoreText.text.Replace(completedStagesCountCode, User.CurrentCategory.CompletedStagesCount.ToString());
        scoreText.text = scoreText.text.Replace(totalStagesCountCode, User.CurrentCategory.Data.stages.ToString());
    }
    
}
