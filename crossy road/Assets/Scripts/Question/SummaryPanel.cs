﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryPanel : MonoBehaviour
{
    [SerializeField] private SummaryStageBoard summaryBoard = null;
    [SerializeField] private Text stageStreakText = null;
    [SerializeField] private Text totalScoreText = null;
    [SerializeField] private string stageStreakWildcard = "{ss}";

    private string originalStreakText;


    private void Awake()
    {
        originalStreakText = stageStreakText.text;
    }
  
    public void Set(StageSummary summary)
    {
        gameObject.SetActive(true);
        summaryBoard.Set(summary);
        string stageStreakPercentage = $"{GetStageStreakPercentage(summary.CorrectQuestionsCount, summary.QuestionsCount)}%";
        stageStreakText.text = originalStreakText.Replace(stageStreakWildcard, stageStreakPercentage);
    }

    public void UpdateTotalScore(int totalScore)
    {
        totalScoreText.text = totalScore.ToString();
    }

    private int GetStageStreakPercentage(int correctQuestionsCount, int questionsCount)
    {
        float streakFactor = (float)correctQuestionsCount / questionsCount;
        float streakPercentage = streakFactor * 100;
        return (int)streakPercentage;
    }

}
