﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionContainer : MonoBehaviour
{
    [SerializeField] private Text optionText = null;
    [SerializeField] private Button optionBtn = null;
    [field: SerializeField] public Image ContainerImg { get; private set; }
    [SerializeField] private Sprite correctSpr = null;
    [SerializeField] private Sprite wrongSpr = null;

    [Header("Single choice")]
    [SerializeField] private Text alphabetLetterText = null;

    [Header("Multiple choice")]
    [SerializeField] private Sprite selectedSpr = null;
    [SerializeField] private Toggle checkmark = null; 

    public static event Action<OptionContainer> OnSingleChoiceSubmission = null;
    public static event Action<List<OptionContainer>> OnMultipleChoiceSubmission = null;

    private const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static int CorrectCount { get; set; }
    private static List<OptionContainer> selectedOptions = new List<OptionContainer>();

    private Sprite original;

    private bool multipleChoice;

    private bool isSelected;
    public bool IsSelected
    {
        get => isSelected;
        private set
        {
            isSelected = value;
            ContainerImg.sprite = isSelected ? selectedSpr : original;
            if (isSelected)
            {
                selectedOptions.Add(this);
                HandleMultipleChoiceSelect();
            }
            else
                selectedOptions.Remove(this);
        }
    }

    public bool IsCorrect { get; private set; }
    public string Feedback { get; private set; }

    private void Awake()
    {
        optionBtn.onClick.AddListener(HandleOptionSelected);
        original = ContainerImg.sprite;
        checkmark.onValueChanged.AddListener((selected) => IsSelected = selected);
    }

    private void OnDestroy()
    {
        optionBtn.onClick.RemoveListener(HandleOptionSelected);
    }


    public void SetAnswer(Option option, bool multipleChoice, int i)
    {
        ResetValues();
        optionText.text = option.text;
        IsCorrect = option.rightAnswer;
        if (IsCorrect)
            CorrectCount++;
        Feedback = option.feedback.text;
        this.multipleChoice = multipleChoice;
        alphabetLetterText.text = multipleChoice ? string.Empty : alphabet[i].ToString();
        checkmark.gameObject.SetActive(multipleChoice);
    }

    private void ResetValues()
    {
        ContainerImg.sprite = original;
        SetAvailability(true);
        checkmark.isOn = false;
        gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        optionText.text = string.Empty;
        Feedback = null;
        IsCorrect = IsSelected = false;
    }

    private void HandleOptionSelected()
    {
        if (multipleChoice)
            checkmark.isOn = !checkmark.isOn;
        else
            HandleSingleChoiceSelect();
    }

    private void HandleMultipleChoiceSelect()
    {
        if (selectedOptions.Count != CorrectCount) return;
        foreach (var option in selectedOptions)
            option.Review();
        OnMultipleChoiceSubmission?.Invoke(selectedOptions);

    }

    private void HandleSingleChoiceSelect()
    {
            Review();
            OnSingleChoiceSubmission?.Invoke(this);
    }


    public void Review()
    {
        ContainerImg.sprite = IsCorrect ? correctSpr : wrongSpr;
    }

    public void SetAvailability(bool enabled)
    {
        optionBtn.interactable = enabled;
        if (!enabled)
            optionText.text = string.Empty;
    }

    public Option ToOption() => new Option { feedback = new Feedback { text = Feedback }, rightAnswer = IsCorrect, text = optionText.text };

}

public static class ListOptionContainer
{
    public static Option[] ToOptions(this List<OptionContainer> optionContainers)
    {
        var options = new Option[optionContainers.Count];
        for (int i = 0; i < options.Length; i++)
            options[i] = optionContainers[i].ToOption();

        return options;
    }

}

