using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CronometerDisplay : MonoBehaviour
{
    [SerializeField] private Text cronometerText = null;
    [SerializeField] private Cronometer cronometer = null;

    private void Awake()
    {
        cronometer.OnTimeUpdated += UpdateTime;
    }

    private void OnDestroy()
    {
        cronometer.OnTimeUpdated -= UpdateTime;
    }

    private void UpdateTime(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)(time % 60);
        //int miliseconds = (int)(((time % 60) - seconds) * 100);
        cronometerText.text = $"{minutes:00}:{seconds:00}"; //.{miliseconds:00}";
    }
}