﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesStageHandler : MonoBehaviour
{
    [SerializeField] private int initialLives = 3;
    [SerializeField] private LivesController livesController = null;

    public static event Action<StageSummary> OnAllLivesLost = null;

    private StageSummary stageSummary;

    private void Awake()
    {
        StageSummary.OnSummaryDataUpdated += HandleStageSummaryUpdated;
    }

    private void Start()
    {
    }

    public void RemoveLive() => livesController.RemoveLives(1);

    private void OnDestroy()
    {
        StageSummary.OnSummaryDataUpdated -= HandleStageSummaryUpdated;
    }

    private void HandleStageSummaryUpdated(StageSummary stageSummary)
    {
        this.stageSummary = stageSummary;
    }



}
