﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesDisplay : MonoBehaviour
{
    [SerializeField] LivesController livesController = null;
    [SerializeField] private Text liveText = null;

    private void Awake()
    {
        livesController.OnLivesUpdated.AddListener(HandleLivesUpdated);
    }

    private void Start()
    {
        HandleLivesUpdated();
    }
    
    private void HandleLivesUpdated()
    {
        liveText.text = livesController.LivesAmount.ToString();
    }
}
