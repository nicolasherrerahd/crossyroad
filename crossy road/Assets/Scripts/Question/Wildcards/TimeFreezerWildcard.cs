﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeFreezerWildcard : Wildcard
{
    [SerializeField] private Timer timer = null;
    [SerializeField] private float freezingTime = 10f;

    public void FreezeTime()
    {
        if (used) return;
        StartCoroutine(CallTimeFreeze());
    }

    private IEnumerator CallTimeFreeze()
    {
        used = true;
        timer.PauseTimer(true);
        yield return new WaitForSeconds(freezingTime);
        timer.PauseTimer(false);
    }

}
