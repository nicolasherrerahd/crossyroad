﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wildcard : MonoBehaviour
{
    [SerializeField] protected Button button;
    protected bool used;

    public void ResetWildcard()
    {
        used = false;
    }

}
