﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageHandler : MonoBehaviour
{
    [SerializeField] private float dwellUntilNextQuestion = 1f;
    [SerializeField] private QuestionContainer questionContainer = null;
    [SerializeField] private List<QuestionData> questions = new List<QuestionData>();
    [SerializeField] private List<DwellEvent> onCorrectOption = new List<DwellEvent>();
    [SerializeField] private List<DwellEvent> onWrongOption = new List<DwellEvent>();
    [SerializeField] private List<DwellEvent> onQuestionUpdate = new List<DwellEvent>();
    [SerializeField] private List<DwellEvent> onQuestionReady = new List<DwellEvent>();
    [SerializeField] private UnityEngine.Events.UnityEvent onStageCompleted = null;

    public static event Action<QuestionData> OnQuestionUpdated = null;
    public static event Action<int, int> OnProgressUpdated = null;

    public bool Stop { get; set; }

    private List<QuestionData> usedQuestions = new List<QuestionData>();
    private int currentQuestionProgress;
    private QuestionData CurrentQuestion => usedQuestions.GetLastItem();
    private StageSummary stageSummary;

    public GameResourcesObject GetGameResourcesObject(int totalScore, int totalBonus)
    {
        return stageSummary.ToGameResourcesObj(totalScore, totalBonus);
    }

    private void Awake()
    {
        OptionContainer.OnSingleChoiceSubmission += HandleSingleChoiceSubmission;
        OptionContainer.OnMultipleChoiceSubmission += HandleMultipleChoiseSubmission;
    }

    private void Start()
    {
        SetQuestions();
        SetStageSummary();
        UpdateQuestion();
    }

    private void OnDestroy()
    {
        OptionContainer.OnSingleChoiceSubmission -= HandleSingleChoiceSubmission;
        OptionContainer.OnMultipleChoiceSubmission -= HandleMultipleChoiseSubmission;
    }

    private void SetStageSummary()
    {
        bool enoughQuestions = User.CurrentCategory != null && User.CurrentCategory.Data.questionsByStage <= questions.Count;
        int questionsPerStage = enoughQuestions ? User.CurrentCategory.Data.questionsByStage : questions.Count;
        stageSummary = new StageSummary(questionsPerStage);

    }

    private void SetQuestions()
    {
        if (User.Questions.Count < 1) return;
        questions = new List<QuestionData>(User.Questions);
    }


    private void HandleSingleChoiceSubmission(OptionContainer option)
    {
        stageSummary.AddQuestionSummary(new QuestionSummary(CurrentQuestion, option));
        if (option.IsCorrect)
            HandleRightOption();
        else
            HandleWrongOption();
    }

    private void HandleMultipleChoiseSubmission(List<OptionContainer> selectedOptions)
    {
        bool isCorrect = selectedOptions.Count == selectedOptions.FindAll(o => o.IsCorrect).Count;
        stageSummary.AddQuestionSummary(new QuestionSummary(CurrentQuestion, selectedOptions, isCorrect));
        if (isCorrect)
            HandleRightOption();
        else
            HandleWrongOption();
    }

    public void AddCurrentQuestionToSummary(bool isCorrect) => stageSummary.AddQuestionSummary(new QuestionSummary(CurrentQuestion, isCorrect));

    private void HandleRightOption()
    {
        User.CurrentCategory?.AddCorrectQuestion(CurrentQuestion);
        StartCoroutine(CallQuestionUpdate(onCorrectOption));
    }

    public void HandleWrongOption()
    {
        StartCoroutine(CallQuestionUpdate(onWrongOption));
    }

    public void MarkSummaryComplete() => stageSummary.Completed = true;

    private void UpdateQuestion()
    {
        if (currentQuestionProgress == stageSummary.QuestionsCount || questions.Count == 0)
        {
            //User.CurrentCategory?.MarkCurrentStageComplete(usedQuestions);
            //OnStageCompleted?.Invoke(stageSummary)
            onStageCompleted?.Invoke();
            return;
        }
        currentQuestionProgress++;
        usedQuestions.Add(questions.GetRandomItem(true));
        OnQuestionUpdated?.Invoke(CurrentQuestion);
        questionContainer.SetQuestion(CurrentQuestion);
        //Debug.Log($"{CurrentQuestion.id}\n{CurrentQuestion.GetCorrectOptions()[0].text}\nMultiresponse: {CurrentQuestion.multiresponse}");
        OnProgressUpdated?.Invoke(currentQuestionProgress, stageSummary.QuestionsCount);
        StartCoroutine(CallDwellEvents(onQuestionReady));
    }

    private IEnumerator CallDwellEvents(List<DwellEvent> dwellEvents)
    {
        foreach (var dwellEvent in dwellEvents)
        {
            dwellEvent.actionEvent?.Invoke();
            yield return new WaitForSeconds(dwellEvent.delay);
        }
    }

    private IEnumerator CallQuestionUpdate(List<DwellEvent> dwellEvents)
    {
        bool noDwellInEvents = dwellEvents.FindAll(a => a.delay == 0).Count == dwellEvents.Count;
        if (dwellEvents.Count == 0 || noDwellInEvents)
            yield return new WaitForSeconds(dwellUntilNextQuestion);
        yield return CallDwellEvents(dwellEvents);
        yield return CallDwellEvents(onQuestionUpdate);
        if (Stop) yield break;
        UpdateQuestion();
    }

}

public class StageSummary
{
    public static event Action<StageSummary> OnSummaryDataUpdated = null;
    public int QuestionsCount { get; private set; }
    public int CorrectQuestionsCount => questionSummaries.FindAll(q => q.ChoseCorrectOption).Count;
    public bool Completed { get; set; }
    public QuestionSummary LastQuestionSummary
    {
        get
        {
            if (questionSummaries.Count > 0)
                return questionSummaries[questionSummaries.Count - 1];
            else
                return null;
        }
    }

    public StageSummary(int initialQuestionsCount)
    {
        QuestionsCount = initialQuestionsCount;
        OnSummaryDataUpdated?.Invoke(this);
    }

    private List<QuestionSummary> questionSummaries = new List<QuestionSummary>();

    public IList<QuestionSummary> QuestionSummaries => questionSummaries.AsReadOnly();
    public GameResourcesObject ToGameResourcesObj(int totalPoints, int totalBonus)
    {
        var gameResourceItems = new GameResourceItem[questionSummaries.Count];
        for (int i = 0; i < gameResourceItems.Length; i++)
        {
            gameResourceItems[i] = questionSummaries[i].ToGameResource();
        }

        var gameResourcesObj = new GameResourcesObject
        {
            points = totalPoints,
            bonus = totalBonus,
            completed = Completed,
            listResource = gameResourceItems
        };
        return gameResourcesObj;
    }

    public void AddQuestionSummary(QuestionSummary question)
    {
        questionSummaries.Add(question);
        OnSummaryDataUpdated?.Invoke(this);
    }
}

public class QuestionSummary
{
    public int Id { get; private set; }
    public int ResourceTypeId { get; private set; }
    public string QuestionTitle { get; private set; }
    public Option CorrectOption { get; private set; }
    public Option[] SelectedOptions { get; private set; }
    public bool ChoseCorrectOption { get; private set; }
    public string Feedback { get; private set; }
    public int Score { get; set; }

    public QuestionSummary(QuestionData currentQuestion, OptionContainer option)
    {
        Id = (int)currentQuestion.id;
        ResourceTypeId = currentQuestion.resourceTypeId;
        QuestionTitle = currentQuestion.text;
        CorrectOption = currentQuestion.GetCorrectOptions()[0];
        ChoseCorrectOption = option.IsCorrect;
        Feedback = option.Feedback == null ? CorrectOption.feedback.text : option.Feedback;
    }

    public QuestionSummary(QuestionData currentQuestion, bool isCorrect)
    {
        Id = (int)currentQuestion.id;
        ResourceTypeId = currentQuestion.resourceTypeId;
        QuestionTitle = currentQuestion.text;
        CorrectOption = currentQuestion.GetCorrectOptions()[0];
        Feedback = CorrectOption.feedback.text;
        ChoseCorrectOption = isCorrect;
    }

    public QuestionSummary(QuestionData currentQuestion, List<OptionContainer> selectedOptions, bool isCorrect)
    {
        Id = (int)currentQuestion.id;
        ResourceTypeId = currentQuestion.resourceTypeId;
        QuestionTitle = currentQuestion.text;
        SelectedOptions = selectedOptions.ToOptions();
        Feedback = GetFeedback(SelectedOptions);
        ChoseCorrectOption = isCorrect;
    }

    public GameResourceItem ToGameResource()
    {
        return new GameResourceItem()
        {
            resourceId = Id,
            typeresourceId = ResourceTypeId,
            points = Score,
            completed = ChoseCorrectOption
        };
    }

    private static string GetFeedback(Option[] options)
    {
        string feedback = $"- {options[0].feedback.text}";
        for (int i = 1; i < options.Length; i++)
            feedback += $"\n- {options[i].feedback.text}";
        return feedback;
    }
}


