﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [field: SerializeField] public int StartingTime { get; set; }
    [SerializeField] private bool stopped = true;
    [SerializeField] public UnityEvent onTimeUp = null; 

    public event Action<int> OnTimeUpdated = null;

    private bool paused;

    private int currentTime;

    private float oneSecondTimer;

    private void Start()
    {
        if (stopped) return;
        RestartTimer();
    }

    public float NormalizedTime => (float)CurrentTime / StartingTime;


    public int CurrentTime
    {
        get => currentTime;
        private set
        {
            if (value < 0) return;
            currentTime = value;
            OnTimeUpdated?.Invoke(currentTime);
            if (currentTime == 0)
                onTimeUp?.Invoke();
        }
    }

    public void StartTimer(int time)
    {
        stopped = paused = false;
        CurrentTime = StartingTime = time;
        oneSecondTimer = 0;
    }

    public void RestartTimer() => StartTimer(StartingTime);

    public void Stop() => stopped = true;

    public void PauseTimer(bool paused) => this.paused = paused;

    private void Update()
    {
        if (paused || stopped) return;
        if (oneSecondTimer < 1f)
            oneSecondTimer += Time.deltaTime;
        else
        {
            oneSecondTimer = 0f;
            CurrentTime--;
        }
    }
}
