﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LivesController : MonoBehaviour
{
    [field: SerializeField] public int InitialLives { get; private set; }
    [field: SerializeField] public UnityEvent OnLivesUpdated { get; private set; }
    [field: SerializeField] public UnityEvent OnLivesAdded { get; private set; }
    [field: SerializeField] public UnityEvent OnLivesRemoved { get; private set; }
    [field: SerializeField] public UnityEvent OnAllLivesLost { get; private set; }

    private void Awake()
    {
        LivesAmount = InitialLives;
    }


    public int LivesAmount { get; private set; }

    public void AddLives(int amount)
    {
        LivesAmount = Mathf.Min(LivesAmount + amount, InitialLives);
        HandleLivesUpdated();
        OnLivesAdded?.Invoke();
    }

    public void RemoveLives(int amount)
    {
        LivesAmount = Mathf.Max(LivesAmount - amount, 0);
        HandleLivesUpdated();
        if (LivesAmount == 0) return;
        OnLivesRemoved?.Invoke();
    }

    private void HandleLivesUpdated()
    {
        OnLivesUpdated?.Invoke();
        if (LivesAmount == 0)
            OnAllLivesLost?.Invoke();
    }
}
