﻿using CrossyRoad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private Text scoreText = null;
    [SerializeField] private VarReplacer replacer = new VarReplacer("{s} pts");

    private void Awake()
    {
        ScoreHandler.OnScoreUpdated += HandleScoreUpdated;
    }

    private void OnDestroy()
    {
        ScoreHandler.OnScoreUpdated -= HandleScoreUpdated;
    }

    private void HandleScoreUpdated(int newScore)
    {
        scoreText.text = replacer.GetReplacedText(newScore.ToString());
    }
}
