﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSizer : MonoBehaviour
{
    [SerializeField] private Text[] texts = new Text[0];
    [SerializeField] private Canvas canvas = null;

    public void AdjustTextSizes()
    {
        StartCoroutine(CallAdjustTextSizes());
    }

    private IEnumerator CallAdjustTextSizes()
    {
        yield return null;
        int smallestTextSize = GetSmallestSize();
        foreach (var text in texts)
        {
            text.resizeTextForBestFit = false;
            text.fontSize = smallestTextSize;
        }
    }

    public void ResetBestFit()
    {
        foreach (var text in texts)
        {
            text.resizeTextForBestFit = true;
        }
    }

    private int GetSmallestSize()
    {
        var smallest = texts[0];
        for (int i = 1; i < texts.Length; i++)
        {
            if (string.IsNullOrWhiteSpace(texts[i].text)) continue;
            //Debug.Log($"Smallest: {smallest.cachedTextGenerator.fontSizeUsedForBestFit}\nText: {texts[i].cachedTextGenerator.fontSizeUsedForBestFit}");
            if (smallest.cachedTextGenerator.fontSizeUsedForBestFit > texts[i].cachedTextGenerator.fontSizeUsedForBestFit)
                smallest = texts[i];
        }
        return (int)(smallest.cachedTextGenerator.fontSizeUsedForBestFit / canvas.scaleFactor);
    }

}
