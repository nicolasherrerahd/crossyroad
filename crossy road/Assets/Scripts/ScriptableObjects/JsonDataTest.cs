using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Json Data Test", menuName = "Custom/JsonDataTest")]
public class JsonDataTest : ScriptableObject
{
    [field: SerializeField, TextArea(5, 30)] public string Json { get; private set; }
}
