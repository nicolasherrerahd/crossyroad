using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingLoader : MonoBehaviour
{
    [SerializeField] private ScoreRankLoader scoreRankLoader = null;
    [SerializeField] private UsernamesLoader usernamesLoader = null;

    public int FullTop => scoreRankLoader.Top;

    public RankingData[] RowsData { get; private set; } = new RankingData[0];

    public event Action OnLoaded = null;

    private void Awake()
    {
        scoreRankLoader.OnLoaded += LoadUsernames;
        usernamesLoader.OnLoaded += AddUsernamesInRowsData;
    }

    public void Load()
    {
        scoreRankLoader.Load();
    }
    private void LoadUsernames(RankingData[] scoreRank)
    {
        RowsData = scoreRank;
        int[] listIds = RankingData.GetListIds(scoreRank);
        usernamesLoader.Load(listIds);
    }

    private void AddUsernamesInRowsData(string[] usernames)
    {
        for (int i = 0; i < usernames.Length; i++)
        {
            RowsData[i].Name = usernames[i];
        }
        OnLoaded?.Invoke();
    }

}
