using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StatsUpdater : MonoBehaviour
{
    [SerializeField] private int objectIdType = 101;
    [SerializeField] private UrlPath statsReportPath = new UrlPath("insertgameresources/");

    public void UpdateStats(GameResourcesObject resourcesObject)
    {
        resourcesObject.objectidtype = objectIdType;
        RestApiObject obj = new RestApiObject
        {
            uri = statsReportPath.Url,
            token = User.Token,
            bodyJson = resourcesObject,
            onSuccess = HandleStatsUpdated
        };
        RestApiRequest.Instance.PostJson(obj);
    }

    private void HandleStatsUpdated(DownloadHandler response)
    {
    }
}
