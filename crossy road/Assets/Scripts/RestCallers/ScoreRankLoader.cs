using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ScoreRankLoader : MonoBehaviour
{
    [SerializeField] private UrlPath path = new UrlPath("ranklistinfocompletedgame/");
    [SerializeField] private int rankingType = 5;
    [SerializeField] private int top = 3;
    [SerializeField] private int maxPositionsAbove = 4;

    public int Top => top + maxPositionsAbove + 1;


    public event Action<RankingData[]> OnLoaded = null;

    public void Load()
    {
        var param = new RankingParam
        {
            typefilter = rankingType,
            userid = (int)User.Id,
            game = SessionData.GameId,
            objectid = SessionData.ConfigId,
            orgid = (int)User.OrgId
        };

        var obj = new RestApiObject
        {
            uri = path.Url,
            token = User.Token,
            bodyJson = param,
            onSuccess = HandleRankingLoaded
        };
        RestApiRequest.Instance.PostJson(obj);
    }

    private void HandleRankingLoaded(DownloadHandler response)
    {
        var scoreRankData = JSON.Parse(response.text);
        var fullScoreRank = new List<RankingData>();
        int userIndex = 0;
        for (int i = 0; i < scoreRankData.Count; i++)
        {
            var rankingData = new RankingData(scoreRankData[i], i);
            fullScoreRank.Add(rankingData);
            Debug.Log(rankingData.ToString());
            if (rankingData.Id != User.Id) continue;
            userIndex = i;
        }
        var scoreRank = new List<RankingData>();
        if(userIndex == 0 || userIndex < top + maxPositionsAbove)
            scoreRank.AddRange(GetRank(fullScoreRank, 0, Top));
        else
        {
            scoreRank.AddRange(GetRank(fullScoreRank, 0, top));
            scoreRank.AddRange(GetPositionsAbove(fullScoreRank, userIndex));
        }
        OnLoaded?.Invoke(scoreRank.ToArray());
    }

    private List<RankingData> GetPositionsAbove(List<RankingData> fullScoreRank, int userIndex)
    {
        var data = new List<RankingData>();
        for (int i = userIndex; i >= userIndex - maxPositionsAbove; i--)
        {
            if (fullScoreRank[i].Position <= top) break;
            data.Add(fullScoreRank[i]);
        }
        data.Reverse();
        return data;
    }

    private List<RankingData> GetRank(List<RankingData> fullScoreRank, int initialIndex, int maxCount)
    {
        var data = new List<RankingData>();
        for (int i = initialIndex; i < fullScoreRank.Count; i++)
        {
            if (data.Count == maxCount) break;
            data.Add(fullScoreRank[i]);
        }
        return data;
    }
}
