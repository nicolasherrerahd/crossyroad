using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class StageLoader : MonoBehaviour
{
    [SerializeField] private VarReplacer infoConfigPath = new VarReplacer("getgameconfigbyid/{idGameConfig}");
    [SerializeField] private QuestionsLoader questionsLoader = null;
    [SerializeField] private UnityEvent onLoaded = null;

    private UrlPath InfoConfigPath => new UrlPath(infoConfigPath.GetReplacedText(SessionData.ConfigId));

    private void Awake()
    {
        questionsLoader.OnLoadFinished += HandleQuestionsLoaded;
    }

    public void LoadStage()
    {
        RestApiObject obj = new RestApiObject
        {
            uri = InfoConfigPath.Url,
            token = User.Token,
            onSuccess = HandleInfoConfigLoaded
        };
        RestApiRequest.Instance.GetJson(obj);
    }

    private void HandleInfoConfigLoaded(DownloadHandler response)
    {
        questionsLoader.Load(JSON.Parse(response.text));
    }

    private void HandleQuestionsLoaded(List<QuestionData> questions)
    {
        User.LoadQuestions(questions);
        onLoaded?.Invoke();
    }
}
