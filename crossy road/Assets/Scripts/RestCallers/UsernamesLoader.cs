using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UsernamesLoader : MonoBehaviour
{
    [SerializeField] private UrlPath path = new UrlPath("filluserlist/");


    int[] ids;

    public event Action<string[]> OnLoaded = null;

    public void Load(int[] listIds)
    {
        ids = listIds;
        var obj = new RestApiObject
        {
            uri = path.Url,
            token = User.Token,
            bodyJson = new UserList { userList = listIds, organizationId = (int)User.OrgId },
            onSuccess = HandleUsernamesLoaded
        };
        RestApiRequest.Instance.PostJson(obj);
    }

    private void HandleUsernamesLoaded(DownloadHandler response)
    {
        var usersData = JSON.Parse(response.text);
        var usernameIds = new Dictionary<int, string>();
        foreach (var item in usersData)
        {
            usernameIds.Add(item.Value["id"], $"{item.Value["firstname"].Value} {item.Value["lastname"].Value}");
        }
        var usernames = new string[ids.Length];
        for (int i = 0; i < usernames.Length; i++)
        {
            usernames[i] = usernameIds[ids[i]];
        }
        OnLoaded?.Invoke(usernames);
    }
}
