using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class QuestionsLoader : MonoBehaviour
{
    [SerializeField]
    private QuestionModuleUrl[] modulePaths = new QuestionModuleUrl[]
    {
        new QuestionModuleUrl("getlistquestionbylistid/", 1),
        new QuestionModuleUrl("gettriviaquestionbylistid/", 2),
        new QuestionModuleUrl("gettriviaquestionbylistidcert/", 3)
    };

    public event System.Action<List<QuestionData>> OnLoadFinished = null;

    private List<QuestionData> questions = new List<QuestionData>();

    JSONNode questionModules;
    private int currentModuleIndex;

    public void Load(JSONNode questionModules)
    {
        currentModuleIndex = 0;
        questions.Clear();
        this.questionModules = questionModules;
        LoadModule(currentModuleIndex);
    }

    private void HandleCategoryQuestionsLoaded(DownloadHandler response)
    {
        var questionsData = JSON.Parse(response.text);
        foreach (var question in questionsData)
        {
            var questionData = JsonUtility.FromJson<QuestionData>(question.Value.ToString());
            questionData.resourceTypeId = modulePaths[currentModuleIndex].ResourceTypeId;
            Debug.Log($"Retrieving question:\nId: {questionData.id}\nResourceTypeId: {questionData.resourceTypeId}");
            questions.Add(questionData);
        }
        currentModuleIndex++;
        CheckAllModulesLoaded();
    }

    private void LoadModule(int moduleIndex)
    {
        var restApiObject = new RestApiObject
        {
            token = User.Token,
            onSuccess = HandleCategoryQuestionsLoaded
        };
        restApiObject.uri = modulePaths[moduleIndex].UrlRoot.Url;
        restApiObject.bodyJson = new QuestionIdList(modulePaths[moduleIndex].ResourceTypeId, questionModules);
        RestApiRequest.Instance.PostJson(restApiObject);
    }

    private void CheckAllModulesLoaded()
    {
        if (currentModuleIndex < modulePaths.Length)
            LoadModule(currentModuleIndex);
        else
            OnLoadFinished?.Invoke(questions);
    }

}
