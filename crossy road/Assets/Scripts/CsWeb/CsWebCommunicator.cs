using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class CsWebCommunicator : MonoBehaviour
{
    [SerializeField] private bool testLog = false;
#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    public static extern void AlertLog(bool succeeded, string logStr);

    [DllImport("__Internal")]
    public static extern void Communicate(string commObj);

    [DllImport("__Internal")]
    public static extern void TestLog(bool isLogging);

    private void Start()
    {
        TestLog(testLog);
    }
#endif

}

[System.Serializable]
public class CommObj
{
    public bool completed;
    public long lessonId;
    public bool getData;

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

    public string ToJson() => ToString();
}
