using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UserTokenRetriever : MonoBehaviour
{
    [SerializeField] private UrlPath path = new UrlPath("auth/gettokenloginvalidate");

    [SerializeField] private WebDataRetriever webRetriever = null;

    public event Action OnUserTokenRetrieved = null;

    private void Awake()
    {
        webRetriever.OnDataRetrieved += HandleUserDataRetrieved;
    }

    private void HandleUserDataRetrieved(JSONNode data)
    {
        var userData = new UserData(data);
        Debug.Log($"Id: {data["userid"]}\nCode: {data["code"]}");
        //{
        //    userid = data["userid"],
        //    code = data["code"].Value
        //};
        Debug.Log($"Id: {userData.userid}\nCode: {userData.code}");
        var obj = new RestApiObject
        {
            uri = path.Url,
            bodyJson = userData,
            onSuccess = HandleSuccess
        };
        RestApiRequest.Instance.PostJson(obj);
    }

    private void HandleSuccess(DownloadHandler response)
    {
        if (string.IsNullOrWhiteSpace(response.text)) return;
        SetUserToken(response.text);
    }

    public void SetUserToken(string json)
    {
        var tokenData = JsonUtility.FromJson<UserData>(json);
        User.LogIn(tokenData);
        OnUserTokenRetrieved?.Invoke();
        //Debug.Log($"Token retrieved successfully! {User.Token}");

    }
}
