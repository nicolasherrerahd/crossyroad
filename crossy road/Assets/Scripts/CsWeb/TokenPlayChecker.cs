using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TokenPlayChecker : MonoBehaviour
{
    [SerializeField] private UserTokenRetriever tokenRetriever = null;
    [SerializeField] private Button playBtn = null;

    private static bool tokenRetrieved;

    private void Awake()
    {
        tokenRetriever.OnUserTokenRetrieved += HandleTokenRetrieved;
    }

    private void Start()
    {
        if (tokenRetrieved) return;
        playBtn.interactable = false;
    }

    private void HandleTokenRetrieved()
    {
        tokenRetrieved = playBtn.interactable = true;
    }
}
