using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class WebDataRetriever : MonoBehaviour
{
    public event Action<JSONNode> OnDataRetrieved = null;

    private void Start()
    {
        RetrieveTokenJsonData();
    }

    private void RetrieveTokenJsonData()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        var commObj = new CommObj { getData = true };
        CsWebCommunicator.Communicate(commObj.ToJson());
#endif
    }

    public void SetData(string jsonStr)
    {
        var json = JSON.Parse(jsonStr);
        Debug.Log(jsonStr);
        SessionData.Set(json);
        OnDataRetrieved?.Invoke(json);
    }
}
