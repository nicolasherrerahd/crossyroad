using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseHandler : MonoBehaviour
{
    bool paused;

    public static event Action<bool> OnPause = null;

    public void TogglePause()
    {
        paused = !paused;
        OnPause?.Invoke(paused);
        UpdateTimeScale();
    }

    private void UpdateTimeScale()
    {
        Time.timeScale = paused ? 0 : 1;
    }


    private void OnDestroy()
    {
        Time.timeScale = 1;
    }
}
