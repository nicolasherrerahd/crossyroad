using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace CrossyRoad
{
    public class SummaryDisplay : MonoBehaviour
    {
        [SerializeField] private GameController gameController = null;
        [SerializeField] private GameObject panel = null;
        [SerializeField] private Image completedImg = null;
        [SerializeField] private Sprite completedSpr = null;
        [SerializeField] private Sprite uncompletedSpr = null;
        [SerializeField] private Timer timer = null;
        [SerializeField] private Text statsText = null;
        [SerializeField] private VarReplacer statsReplacer = new VarReplacer("");
        [SerializeField] private Color bonusTimeColor = Color.green;
        [SerializeField] private Text totalScoreText = null;
        [SerializeField] private VarReplacer totalScoreReplacer = new VarReplacer("");

        private StageSummary stageSummary;

        private void Awake()
        {
            gameController.OnGameFinished += HandleGameFinished;
            StageSummary.OnSummaryDataUpdated += HandleStageSummaryUpdated;
        }

        private void OnDestroy()
        {
            gameController.OnGameFinished -= HandleGameFinished;
            StageSummary.OnSummaryDataUpdated -= HandleStageSummaryUpdated;
        }

        private void HandleGameFinished(SummaryStats summary)
        {
            panel.SetActive(true);
            completedImg.sprite = gameController.StageFinished ? completedSpr : uncompletedSpr;
            object[] stats = new object[]
            {
                stageSummary.CorrectQuestionsCount,
                stageSummary.QuestionsCount,
                summary.CorrectQuestionsBonus,
                summary.ArrivedPeopleBonus,
                TimerDisplay.GetTimeStr(timer.CurrentTime),
                summary.TimeBonus > 0 ? bonusTimeColor.ToRichText($"+{summary.TimeBonus}") : summary.TimeBonus.ToString(),
                summary.RemainingAttemptsBonus > 0 ? bonusTimeColor.ToRichText($"+{summary.RemainingAttemptsBonus}") : summary.RemainingAttemptsBonus.ToString()
            };
            statsText.text = statsReplacer.GetReplacedText(stats);
            totalScoreText.text = totalScoreReplacer.GetReplacedText(summary.TotalScore);
            // TODO update score when timePunishFactor is updated.

        }

        private void HandleStageSummaryUpdated(StageSummary summary)
        {
            stageSummary = summary;
        }
    }
}
