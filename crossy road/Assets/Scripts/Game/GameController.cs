using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CrossyRoad
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private PersonSelector personSelector = null;
        [SerializeField] private CanvasActivator questionCanvasActivator = null;
        [SerializeField] private InputHandler inputHandler = null;
        [SerializeField] private ScoreHandler scoreHandler = null;
        [SerializeField] private StageHandler stageHandler = null;
        [SerializeField] private StatsUpdater statsUpdater = null;
        [SerializeField] private Timer timer = null;

        public bool StageFinished { get; set; }

        public event Action<SummaryStats> OnGameFinished = null;

        private bool gameFinished;


        private void Awake()
        {
            Person.OnArrival += HandlePersonArrival;
            Person.OnArrivalDwellPassed += HandlePersonArrivalDwellPassed;
            Person.OnCollisionWithObstacle += HandleCollisionWithObstacle;
            personSelector.OnAllPeopleDeparted += CheckStageFinished;
            timer.onTimeUp.AddListener(FinishGame);
        }

        private void OnDestroy()
        {
            Person.OnArrival -= HandlePersonArrival;
            Person.OnArrivalDwellPassed -= HandlePersonArrivalDwellPassed;
            Person.OnCollisionWithObstacle -= HandleCollisionWithObstacle;
            personSelector.OnAllPeopleDeparted -= FinishGame;
        }

        private void HandlePersonArrival(Person person)
        {
            timer.PauseTimer(true);
            if (person.IsInvincible || !StageFinished) return;
            scoreHandler.AddPersonBonus(person.transform);
        }

        private void HandlePersonArrivalDwellPassed(Person person)
        {
            timer.PauseTimer(false);
            if (person.IsInvincible || StageFinished) return;
            inputHandler.Enabled = false;
            questionCanvasActivator.SetCanvasActive(true);
        }

        private void HandleCollisionWithObstacle(Person person)
        {
            if (StageFinished) 
                person.Die();
            else
            {
                inputHandler.Enabled = false;
                questionCanvasActivator.SetCanvasActive(true);
            }
        }

        public void CheckStageFinished()
        {
            if (!StageFinished && inputHandler.Person != null && inputHandler.Person.gameObject.activeSelf) return;
            FinishGame();
        }

        public void CheckPeopleCount()
        {
            if (personSelector.RemainingPeople > 0 || inputHandler.Person != null) return;
            FinishGame();
        }

        public void TryEnableInputHandler()
        {
            if (gameFinished) return;
            inputHandler.Enabled = true;
        }

        public void FinishGame()
        {
            gameFinished = true;
            inputHandler.Enabled = false;
            timer.Stop();
            if (StageFinished)
            {
                scoreHandler.AddRemainingAttemptsBonus();
                stageHandler.MarkSummaryComplete();
#if UNITY_WEBGL && !UNITY_EDITOR
            if (SessionData.LessonId > 0)
            {
                var commObj = new CommObj { completed = true, lessonId = SessionData.LessonId };
                CsWebCommunicator.Communicate(commObj.ToJson());
            }
#endif
            }
            var gameObj = stageHandler.GetGameResourcesObject(scoreHandler.Stats.TotalScore, scoreHandler.Stats.TotalBonus);
            statsUpdater.UpdateStats(gameObj);
            OnGameFinished?.Invoke(scoreHandler.Stats);
        }
    }
}
