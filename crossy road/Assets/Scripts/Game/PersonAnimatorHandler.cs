using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class PersonAnimatorHandler : MonoBehaviour
    {
        [SerializeField] private Animator transformationAnim = null;
        [SerializeField] private AnimationClip transformationClip = null;
        [SerializeField] private Animator personAnim = null;
        [SerializeField] private AnimationClip celebrateClip = null;
        [SerializeField] private AnimationClip dieClip = null;
        [SerializeField] private Mover mover = null;


        private void Awake()
        {
            mover.OnMove += HandleMovement;
        }

        private void HandleMovement(Vector3 direction)
        {
            personAnim.SetBool("Walking", Mathf.Abs(direction.normalized.magnitude) > 0f);
        }

        public IEnumerator Celebrate()
        {
            personAnim.Play(celebrateClip.name);
            personAnim.SetBool("Walking", false);
            yield return new WaitForSeconds(celebrateClip.length);
        }

        public IEnumerator Transform()
        {
            transformationAnim.Play(transformationClip.name);
            yield return new WaitForSeconds(transformationClip.length);
        }

        public IEnumerator CelebrateTransition()
        {
            yield return Celebrate();
            yield return Transform();
        }

        public IEnumerator Die()
        {
            personAnim.Play(dieClip.name);
            personAnim.SetBool("Walking", false);
            yield return new WaitForSeconds(dieClip.length);
        }

    }

}
