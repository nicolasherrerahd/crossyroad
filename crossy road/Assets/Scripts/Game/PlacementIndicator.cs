using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class PlacementIndicator : MonoBehaviour
    {
        public void Select(Person person)
        {
            transform.SetParent(person.transform);
            transform.position = person.IndicatorPoint.position;
        }
    }

}
