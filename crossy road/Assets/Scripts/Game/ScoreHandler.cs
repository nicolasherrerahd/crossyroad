using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class ScoreHandler : MonoBehaviour
    {
        [SerializeField] private int bonusPerPerson = 25;
        [SerializeField] private int bonusPerCorrectQuestion = 100;
        [SerializeField] private int bonusPerRemainingAttempt = 50;
        [SerializeField, Range(0f, 1f)] private float percentagePtsOnCollision = 0.5f;
        [SerializeField] private InputHandler inputHandler = null;
        [SerializeField] private LivesController livesController = null;

        public SummaryStats Stats { get; private set; } = new SummaryStats();

        public static event Action<int, SummaryStats, Transform> OnScoreUpdated = null;

        private void Start()
        {
            OnScoreUpdated?.Invoke(0, Stats, null);
        }

        public void AddPersonBonus(Transform personPos)
        {
            Stats.ArrivedPeopleBonus += bonusPerPerson;
            OnScoreUpdated?.Invoke(bonusPerPerson, Stats, personPos);
        }

        public void AddCorrectQuestionBonus(Transform questionPos)
        {
            float bonus = bonusPerCorrectQuestion * (inputHandler.Person.CollidedWithObstacle ? percentagePtsOnCollision : 1);
            int roundedBonus = Mathf.RoundToInt(bonus);
            Stats.CorrectQuestionsBonus += roundedBonus;
            OnScoreUpdated?.Invoke(roundedBonus, Stats, questionPos);
        }

        public void AddRemainingAttemptsBonus()
        {
            Stats.RemainingAttemptsBonus = livesController.LivesAmount * bonusPerRemainingAttempt;
            OnScoreUpdated?.Invoke(0, Stats, null);
        }

        public void UpdateTimeBonusFactor(float punishFactor)
        {
            Stats.TimeBonusFactor = punishFactor;
            OnScoreUpdated?.Invoke(0, Stats, null);
        }
    }
}
