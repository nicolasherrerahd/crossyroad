using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class RecursiveVerticalSprsScroller : VerticalSpritesScroller
    {
        [field: SerializeField] public RectRange ActiveLimitArea { get; private set; } = null;

        protected override void HandleScrolledSpr(ScrollingSprite scrollingSprite)
        {
            CheckSpriteIsOutOfRange(scrollingSprite);
        }

        private void CheckSpriteIsOutOfRange(ScrollingSprite scrollingSprite)
        {
            float posY = scrollingSprite.transform.position.y;
            if (posY >= ActiveLimitArea.MinAxes.y && posY <= ActiveLimitArea.MaxAxes.y) return;
            scrollingSprite.gameObject.SetActive(false);
        }


    }

}
