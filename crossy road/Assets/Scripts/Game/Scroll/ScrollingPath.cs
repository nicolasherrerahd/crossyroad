using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class ScrollingPath : VerticalSpritesScroller
    {
        [SerializeField] private ScrollingSprite[] scrollingSprites = new ScrollingSprite[0];

        private float InitialScrollingSprYPos => topScrollingSpr.transform.position.y - (topScrollingSpr.Units.y * Direction);
        private float Limit => (cam.orthographicSize + (IsSpeedNegative ? 0 : scrollingSprites[0].Units.y)) * Direction;
        private bool IsSpeedNegative => Speed < 0;
        private int Direction =>  IsSpeedNegative ? 1 : -1;

        private Camera cam;
        private ScrollingSprite topScrollingSpr;

        protected override void Start()
        {
            base.Start();
            cam = Camera.main;
            SetInitialPositions();
            ScrollingSprites.AddRange(scrollingSprites);
        }

        protected override void HandleBeforeScroll(ScrollingSprite scrollingSprite)
        {
            if (IsLimitReached(scrollingSprite))
                SnapToInitialScrollingSpr(scrollingSprite);
        }


        private void SetInitialPositions()
        {
            float nextPosY = cam.orthographicSize * Direction;
            foreach (var scrollSpr in scrollingSprites)
            {
                Vector3 newPos = scrollSpr.transform.position;
                newPos.y = nextPosY;
                nextPosY -= scrollSpr.Units.y * Direction;
                scrollSpr.transform.position = newPos;
            }
            topScrollingSpr = scrollingSprites[scrollingSprites.Length - 1];
        }

        private void SnapToInitialScrollingSpr(ScrollingSprite scrollingSprite)
        {
            Vector3 currentPos = scrollingSprite.transform.position;
            currentPos.y = InitialScrollingSprYPos;
            scrollingSprite.transform.position = currentPos;
            topScrollingSpr = scrollingSprite;
        }
        private bool IsLimitReached(ScrollingSprite scrollSpr)
        {
            return IsSpeedNegative ? scrollSpr.transform.position.y > Limit : scrollSpr.transform.position.y < Limit;
        }
    }

}
