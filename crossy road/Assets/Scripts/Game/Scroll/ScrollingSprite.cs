using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class ScrollingSprite : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer refSprRenderer = null;
        [SerializeField] private bool useSprRendererUnits = true;
        [SerializeField] private Vector2 additionalSpeedThreshold = Vector2.zero;

        private float additionalSpeed;

        public Vector2 Units
        {
            get
            {
                if (!useSprRendererUnits) return Vector2.zero;
                Vector2 dimensions = new Vector2(refSprRenderer.sprite.texture.width, refSprRenderer.sprite.texture.height);
                float pixelsPerUnit = refSprRenderer.sprite.pixelsPerUnit;
                return new Vector2(dimensions.x / pixelsPerUnit, dimensions.y / pixelsPerUnit);
            }
        }

        private void OnEnable()
        {
            additionalSpeed = Random.Range(additionalSpeedThreshold.x, additionalSpeedThreshold.y);
        }

        public void Scroll(float speed, float deltaTime)
        {
            transform.position -= new Vector3(0f, (speed + additionalSpeed) * deltaTime, 0f);
        }

        public bool Equals(ScrollingSprite other)
        {
            return refSprRenderer.sprite.Equals(other.refSprRenderer.sprite);
        }

    }

}
