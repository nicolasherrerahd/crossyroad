using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class VerticalSpritesScroller : EnableBehaviour
    {
        [SerializeField] private bool startEnabled = true;
        [field: SerializeField] public float Speed { get; set; } = 7f;

        private float initialSpeed;
        public List<ScrollingSprite> ScrollingSprites { get; } = new List<ScrollingSprite>();

        protected virtual void Start()
        {
            Enabled = startEnabled;
            initialSpeed = Speed;
        }

        private void FixedUpdate()
        {
            foreach (var scrollingSpr in ScrollingSprites)
            {
                if (!Enabled && !scrollingSpr.gameObject.activeSelf) continue;
                HandleBeforeScroll(scrollingSpr);
                scrollingSpr.Scroll(Enabled ? Speed : 0, Time.fixedDeltaTime);
                HandleScrolledSpr(scrollingSpr);
            }
        }

        protected virtual void HandleBeforeScroll(ScrollingSprite scrollingSprite) { }

        protected virtual void HandleScrolledSpr(ScrollingSprite scrollingSprite) { }

        public void ResetSpeed() => Speed = initialSpeed;

    }

}
