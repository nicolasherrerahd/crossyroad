using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CrossyRoad
{
    public class ScrollingSpriteSpawner : EnableBehaviour
    {
        [SerializeField] private RectRange[] spawnZones = null;
        [SerializeField] private RecursiveVerticalSprsScroller verticalScroller = null;
        [SerializeField] private ScrollingSprite[] scrollingSprPrefabs = new ScrollingSprite[0];
        [SerializeField] private float minSpawnRate = 0.7f;
        [SerializeField] private float maxSpawnRate = 1.5f;
        [SerializeField] private bool spawning = true;

        public override bool Enabled
        {
            get => spawning;
            set
            {
                if (spawning == value) return;
                spawning = value;
                if (spawning)
                    StartCoroutine(Start());
            }
        }



        private List<ScrollingSprite> usedPrefabs = new List<ScrollingSprite>();

        private ScrollingSprite lastSpawned;

        private IEnumerator Start()
        {
            while (Enabled)
            {
            foreach (var spawnZone in spawnZones)
                SpawnRandomScrollingSpr(spawnZone);
                yield return new WaitForSeconds(Random.Range(minSpawnRate, maxSpawnRate));
            }
        }

        private void SpawnRandomScrollingSpr(RectRange spawnZone)
        {
            if (usedPrefabs.Count == 0) usedPrefabs.AddRange(scrollingSprPrefabs);
            var randomSpr = usedPrefabs.GetRandomItem(true);
            SpawnRandomScrollingSpr(spawnZone, randomSpr);
        }

        private void SpawnRandomScrollingSpr(RectRange spawnZone, ScrollingSprite sprite)
        {
            bool unused = verticalScroller.ScrollingSprites.TryFind(s => s.Equals(sprite) && !s.gameObject.activeSelf, out var match);
            if (unused)
            {
                match.gameObject.SetActive(true);
                match.transform.position = GetRandomUniquePos(spawnZone, match.Units.x);
                lastSpawned = match;
            }
            else
            {
                Vector3 pos = GetRandomUniquePos(spawnZone, sprite.Units.x);
                Transform parent = verticalScroller.ActiveLimitArea.transform;
                var scrollingSpr = Instantiate(sprite, pos, sprite.transform.rotation, parent);
                verticalScroller.ScrollingSprites.Add(scrollingSpr);
                lastSpawned = scrollingSpr;
            }

        }

        public void SpawnScrollingSpr(int sprIndex)
        {
            SpawnRandomScrollingSpr(spawnZones[0], scrollingSprPrefabs[sprIndex]);
        }

        private Vector3 GetRandomUniquePos(RectRange spawnZone, float unitsX)
        {
                Vector3 pos = spawnZone.GetRandomPosWithinRange(unitsX);
                while (spawnZone.HasRange && lastSpawned != null && Mathf.RoundToInt(pos.x) == Mathf.RoundToInt(lastSpawned.transform.position.x))
                    pos = spawnZone.GetRandomPosWithinRange(unitsX);
            return pos;
        }

    }

}
