using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetector : MonoBehaviour
{
    [SerializeField] private string tagToDetect = "Elevator";

    public bool IsColliding => Collision != null;
    public Collider2D Collision { get; set; }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag(tagToDetect)) return;
        Collision = collision;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag(tagToDetect) || Collision != collision) return;
        Collision = null;
    }
}
