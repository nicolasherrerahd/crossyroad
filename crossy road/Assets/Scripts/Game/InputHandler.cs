using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class InputHandler : MonoBehaviour
    {
        [SerializeField] private GameObject mobileControl = null;
        [SerializeField] private Joystick joystick = null;
        [SerializeField] private GameObject elevatorCeilCollider = null;
        public Person Person
        {
            get => person;
            set
            {
                person = value;
                elevatorCeilCollider.SetActive(false);
            }
        }

        private Person person;

        private Vector3 movement;

        public bool EnableY { get; set; } = true;
        public bool Enabled { get; set; } = true;

        private void Awake()
        {
            Person.OnElevator += HandleElevator;
            mobileControl.SetActive(Device.IsMobileOrTablet);
        }

        private void Update()
        {
            if (Device.IsMobileOrTablet)
            {
                movement.x = Enabled ? joystick.Horizontal : 0;
                movement.y = Enabled && EnableY ? joystick.Vertical : 0;
            }
            else
            {
                movement.x = Enabled ? Input.GetAxisRaw("Horizontal") : 0;
                movement.y = Enabled && EnableY ? Input.GetAxisRaw("Vertical") : 0;
            }
        }

        private void OnDestroy()
        {
            Person.OnElevator -= HandleElevator;
        }

        private void FixedUpdate()
        {
            if (Person == null) return;
            Person.Mover.Move(movement, Time.fixedDeltaTime);
        }

        private void HandleElevator(Person person)
        {
            if (person != Person) return;
            EnableY = !person.IsInElevator;
        }

        public void KillCurrentPerson()
        {
            if (Person.CollidedWithObstacle)
                Person.Die();
        }

        public void MakeCurrentPersonInvincible()
        {
            elevatorCeilCollider.SetActive(true);
            if (Person.CollidedWithObstacle)
                Person.MakeInvincible();
        }
    }
}
