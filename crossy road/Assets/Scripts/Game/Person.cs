using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class Person : MonoBehaviour
    {
        [field: SerializeField] public Mover Mover { get; private set; }
        [field: SerializeField] public Transform IndicatorPoint { get; private set; }
        [SerializeField] private TriggerDetector top = null;
        [SerializeField] private TriggerDetector bottom = null;
        [SerializeField] private PersonAnimatorHandler animatorHandler = null;
        [SerializeField] private GameObject invincibilityAura = null;

        public static event Action<Person> OnArrival = null;
        public static event Action<Person> OnArrivalDwellPassed = null;
        public static event Action<Person> OnCollisionWithObstacle = null;
        public static event Action<Person> OnDeath = null;
        public static event Action<Person> OnElevator = null;

        private bool isInElevator;

        public bool IsInElevator
        {
            get => isInElevator; 
            private set
            {
                isInElevator = value;
                OnElevator?.Invoke(this);
            }
        }

        public bool IsInvincible { get; private set; }
        public bool CollidedWithObstacle { get; private set; }

        public bool FitsInElevator => top.IsColliding && bottom.IsColliding;

        private Transform originalParent;

        private void Start()
        {
            originalParent = transform.parent;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            switch (collision.tag)
            {
                case "Goal":
                    Arrive();
                    break;
                case "Obstacle":
                    if (IsInvincible || CollidedWithObstacle) break;
                    HandleCollisionWithObstacle();
                    break;
                case "Elevator":
                case "Rail":
                    if (IsInvincible || CollidedWithObstacle) break;
                    CheckIsInElevator();
                    break;
                default:
                    break;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            bool isParentDisabling = transform.parent != null && !transform.parent.gameObject.activeSelf;
            if (!collision.CompareTag("Rail") || !gameObject.activeSelf || isParentDisabling) return;
            ResetParent();
            IsInElevator = false;
        }

        private void OnEnable()
        {
            if (!IsInElevator) return;
            IsInElevator = false;
            ResetParent();
        }

        private void ResetParent() => transform.SetParent(originalParent);

        private void CheckIsInElevator()
        {
            if (IsInvincible || CollidedWithObstacle) return;
            if (!FitsInElevator)
                HandleCollisionWithObstacle();
            else
            {

                IsInElevator = true;
                transform.SetParent(top.Collision.transform);
            }
        }

        private void Arrive()
        {
            StartCoroutine(CallArrival());
        }

        private IEnumerator CallArrival()
        {
            Mover.Enabled = false;
            OnArrival?.Invoke(this);
            invincibilityAura.SetActive(false);
            yield return animatorHandler.CelebrateTransition();
            OnArrivalDwellPassed?.Invoke(this);
        }

        private void HandleCollisionWithObstacle()
        {
            CollidedWithObstacle = true;
            OnCollisionWithObstacle?.Invoke(this);
        }

        public void MakeInvincible()
        {
            Debug.Log("Making invincible");
            IsInvincible = true;
            gameObject.SetActive(true);
            invincibilityAura.SetActive(true);
            transform.SetParent(null);
            transform.position = new Vector2(transform.position.x, 0f);
        }

        public void Die()
        {
            gameObject.SetActive(true);
            StartCoroutine(CallDeath());
        }

        private IEnumerator CallDeath()
        {
            Mover.Enabled = false;
            yield return animatorHandler.Die();
            gameObject.SetActive(false);
            OnDeath?.Invoke(this);
        }
    }
}
