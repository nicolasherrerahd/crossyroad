using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace CrossyRoad
{
    public class ScoreDisplay : MonoBehaviour
    {
        [SerializeField] private Text text = null;
        [SerializeField] private VarReplacer replacer = new VarReplacer("{s} pts");

        private void Awake()
        {
            ScoreHandler.OnScoreUpdated += HandleScoreUpdated;
        }

        private void OnDestroy()
        {
            ScoreHandler.OnScoreUpdated -= HandleScoreUpdated;
        }

        private void HandleScoreUpdated(int bonus, SummaryStats stats, Transform transform)
        {
            var scoreValues = new object[]
            {
                stats.GrossScore,
            };
            text.text = replacer.GetReplacedText(scoreValues);
        }
    }

}
