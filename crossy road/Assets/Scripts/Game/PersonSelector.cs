using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class PersonSelector : MonoBehaviour
    {
        [SerializeField] private InputHandler inputHandler = null;
        [SerializeField] private PlacementIndicator placementIndicator = null;
        [SerializeField] private Person[] people = new Person[0];

        public int RemainingPeople => departingPeople.Count;

        private List<Person> departingPeople = new List<Person>();
        private List<Person> deadPeople = new List<Person>();
        private List<Person> arrivingPeople = new List<Person>();

        public event Action OnAllPeopleDeparted = null;

        private void Awake()
        {
            Person.OnDeath += HandleDeath;
            Person.OnArrivalDwellPassed += HandleArrivalDwellPassed;
        }

        private void Start()
        {
            departingPeople.AddRange(people);
            UpdatePerson();
        }

        private void OnDestroy()
        {
            Person.OnDeath -= HandleDeath;
            Person.OnArrivalDwellPassed -= HandleArrivalDwellPassed;
        }

        private void HandleDeath(Person person)
        {
            departingPeople.Remove(person);
            deadPeople.Add(person);
            CheckAllPeopleDeparted();
        }

        private void HandleArrivalDwellPassed(Person person)
        {
            departingPeople.Remove(person);
            arrivingPeople.Add(person);
            CheckAllPeopleDeparted();
        }

        private void CheckAllPeopleDeparted()
        {
            if (departingPeople.Count > 0)
                UpdatePerson();
            else
                HandleAllPeopleDeparted();
        }

        private void UpdatePerson()
        {
            inputHandler.Person = departingPeople.GetRandomItem(true);
            placementIndicator.Select(inputHandler.Person);
            inputHandler.EnableY = true;
        }

        private void HandleAllPeopleDeparted()
        {
            OnAllPeopleDeparted?.Invoke();
            placementIndicator.gameObject.SetActive(false);
            inputHandler.Person = null;
        }

    }

}
