using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class Elevator : MonoBehaviour
    {
        [SerializeField] private Transform pointToSnap = null;
        [SerializeField] private float movingSpeed = 7f;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!collision.TryGetComponent<Person>(out var person) || !person.FitsInElevator || person.IsInvincible) return;
            Vector3 targetPos = new Vector3(person.transform.position.x, pointToSnap.position.y, person.transform.position.z);
            person.transform.position = targetPos;
            //StartCoroutine(MoveTowardsNewPos(person.transform));
        }

        private IEnumerator MoveTowardsNewPos(Transform person)
        {
            Vector3 targetPos = new Vector3(person.position.x, pointToSnap.position.y, person.position.z);
            while (Vector3.Distance(person.position, targetPos) > 0.1f)
            {
                person.position = Vector3.MoveTowards(person.position, targetPos, movingSpeed * Time.fixedDeltaTime);
                yield return new WaitForFixedUpdate();
            }

        }

    }
}
