using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseDisplay : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel = null;

    private void Awake()
    {
        PauseHandler.OnPause += HandlePause;
    }
        
    private void OnDestroy()
    {
        PauseHandler.OnPause -= HandlePause;
    }

    private void HandlePause(bool paused)
    {
        pausePanel.SetActive(paused);
    }
}
