using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrossyRoad
{
    public class PointFeedback : MonoBehaviour
    {
        [SerializeField] private Animator anim = null;
        [SerializeField] private AnimationClip feedbackClip = null;
        [SerializeField] private Text scoreText = null;

        private void Awake()
        {
            ScoreHandler.OnScoreUpdated += Spawn;
        }

        private void OnDestroy()
        {
            ScoreHandler.OnScoreUpdated -= Spawn;
        }


        private void Spawn(int scoreEarned, SummaryStats stats, Transform target)
        {
            if (target == null) return;
            transform.position = target.position;
            scoreText.text = $"+{scoreEarned}";
            anim.PlayInFixedTime(feedbackClip.name, 0, 0f);
        }
    }
}
