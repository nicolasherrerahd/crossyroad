using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrossyRoad
{
    public class BonusDisplay : MonoBehaviour
    {
        [SerializeField] private Text text = null;
        [SerializeField] private VarReplacer replacer = new VarReplacer("+{b}");

        private void Awake()
        {
            ScoreHandler.OnScoreUpdated += HandleScoreUpdated;
        }

        private void OnDestroy()
        {
            ScoreHandler.OnScoreUpdated -= HandleScoreUpdated;
        }

        private void HandleScoreUpdated(int bonus, SummaryStats stats, Transform transform)
        {
            var scoreValues = new object[]
            {
                 $"{(stats.TimeBonus > 0 ? "+" : string.Empty)}{stats.TimeBonus}"
            };
            text.text = replacer.GetReplacedText(scoreValues);
        }
    }

}
