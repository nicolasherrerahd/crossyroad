using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class SummaryStats
    {
        public int CorrectQuestionsBonus { get; set; }
        public int ArrivedPeopleBonus { get; set; }
        public float TimeBonusFactor { get; set; }
        public int GrossScore => ArrivedPeopleBonus + CorrectQuestionsBonus;
        public int TimeBonus => Mathf.RoundToInt(GrossScore * TimeBonusFactor);
        public int TotalBonus => ArrivedPeopleBonus + TimeBonus + RemainingAttemptsBonus;
        public int RemainingAttemptsBonus { get; set; }
        public int TotalScore => GrossScore + TimeBonus + RemainingAttemptsBonus;
    }
}
