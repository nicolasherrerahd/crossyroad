using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrlRoot : MonoBehaviour
{
    [SerializeField] private string root = "https://games.clapdev.com/game/";

    public static string Root { get; private set; }

    private void Start()
    {
        SetRoot(root);
    }

    public void SetRoot(string root) => Root = root;
}
