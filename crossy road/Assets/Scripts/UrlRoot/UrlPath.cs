using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UrlPath
{
    [SerializeField] private string path = null;

    public UrlPath(string path)
    {
        this.path = path;
    }

    public string Url => $"{UrlRoot.Root}{path}";
}
