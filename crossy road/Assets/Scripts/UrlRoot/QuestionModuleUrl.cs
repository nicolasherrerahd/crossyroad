using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestionModuleUrl
{
    [field: SerializeField] public UrlPath UrlRoot { get; private set; }
    [field: SerializeField] public int ResourceTypeId { get; private set; }

    public QuestionModuleUrl()
    {
        
    }

    public QuestionModuleUrl(string url, int resourceTypeId)
    {
        UrlRoot = new UrlPath(url);
        ResourceTypeId = resourceTypeId;
    }
}
