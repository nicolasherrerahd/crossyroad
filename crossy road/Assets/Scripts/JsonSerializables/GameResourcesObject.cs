using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameResourcesObject
{
    public int gameid;
    public long userid;
    public long organizationid;
    public int points;
    public int bonus;
    public int lessonid;
    public bool completed;
    public int objectid;
    public int objectidtype;
    public GameResourceItem[] listResource;
    public bool bestResult;
    public bool withattemp;

    public GameResourcesObject()
    {
        userid = User.Id;
        gameid = SessionData.GameId;
        organizationid = User.OrgId;
        lessonid = SessionData.LessonId;
        objectid = SessionData.ConfigId;
        bestResult = true;
        withattemp = true;
    }
}
