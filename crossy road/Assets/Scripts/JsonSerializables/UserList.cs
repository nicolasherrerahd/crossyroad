using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserList
{
    public int[] userList;
    public int organizationId;
}
