using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameResourceItem
{
    public int resourceId;
    public int typeresourceId;
    public int points;
    public bool completed;
}
