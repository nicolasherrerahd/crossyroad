using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RankingParam
{
    public int typefilter;	//typo de rankins  1: ranking del juego en general   2: rankigs por organizacion y juego, 3: rankigs por organizacion, juego y grupo de matricula, 
    public int userid;
    public int game;	//id del juego
    public int objectid; //id de la configuracion
    public int orgid;	//organizacion
    public int top;
}
