using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringExtensions
{
    public static string ToCamelCase(this string str)
    {
        string firstLetter = str[0].ToString();
        string camelCase = str.Remove(0, 1);
        return $"{firstLetter.ToLower()}{camelCase}";
    }
}
