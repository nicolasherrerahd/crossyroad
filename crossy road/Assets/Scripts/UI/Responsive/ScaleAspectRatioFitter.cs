﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class ScaleAspectRatioFitter : MonoBehaviour
{
    [SerializeField] private float parentFactorToScale = 1f;
    [SerializeField] private bool checkWidth = true;
    [SerializeField] private bool checkHeight = false;
    private RectTransform parent;
    private RectTransform rectTransform;

    private void OnValidate()
    {
        if (parent == null || rectTransform == null)
            SetRectTransforms();
        UpdateRectScale();
    }

    private void SetRectTransforms()
    {
        if (transform.parent == null)
        {
            enabled = false;
            return;
        }
        parent = (RectTransform)transform.parent;
        rectTransform = (RectTransform)transform;
    }

    private void Start()
    {
        SetRectTransforms();
    }

    private void Update()
    {
        UpdateRectScale();
    }

    private void UpdateRectScale()
    {
        if (parent == null || rectTransform == null)
        {
            SetRectTransforms();
            return;
        }
        if (checkWidth)
            CheckWidth();
        if (checkHeight)
            CheckHeight();
    }

    private void CheckWidth()
    {
        float parentWidth = parent.rect.width * parentFactorToScale;
        if (rectTransform.rect.width > parentWidth)
        {
            float scaleFactor = parentWidth / rectTransform.rect.width;
            transform.localScale = Vector3.one * scaleFactor;
        }
        else
            transform.localScale = Vector3.one;
    }

    private void CheckHeight()
    {
        if (rectTransform.rect.height > parent.rect.height)
        {
            float scaleFactor = parent.rect.height / rectTransform.rect.height;
            transform.localScale = Vector3.one * scaleFactor;
        }
        else
            transform.localScale = Vector3.one;
    }
}
