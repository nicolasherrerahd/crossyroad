using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollAimer : MonoBehaviour
{
    [SerializeField] private RectTransform content = null;
    [SerializeField] private Scrollbar bar = null;

    public void AimAt(Transform target)
    {
        //Debug.Log(target.parent == content.transform);
        if (target.parent != content.transform) return;
        int targetIndex = target.GetSiblingIndex();
        float maxIndex = content.childCount - 1;
        bar.value = (maxIndex - targetIndex) / maxIndex;
        //Debug.Log($"{maxIndex - targetIndex}/{maxIndex} = {(maxIndex - targetIndex) / maxIndex}");
    }

}
