using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserDataCodeTest : MonoBehaviour
{
    [SerializeField, TextArea(5, 10)] private string jsonStr = null;
    [SerializeField] private WebDataRetriever webDataRetriever = null;

    private void Start()
    {
        Debug.Log(jsonStr);
        webDataRetriever.SetData(jsonStr);
    }
}