using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurntTokenSetterTest : MonoBehaviour
{
    [SerializeField, TextArea(5, 10)] private string jsonStr = null;
    [SerializeField] private UserTokenRetriever tokenRetriever = null;

    private void Start()
    {
        SessionData.Set(JSON.Parse(jsonStr));
        tokenRetriever.SetUserToken(jsonStr);
    }

}
