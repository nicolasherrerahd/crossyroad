using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserScoreRankingLoadingTest : MonoBehaviour
{
    [SerializeField] private BurntTokenSetterTest tokenSetter = null;
    [SerializeField] private ScoreRankLoader rankLoader = null;

    private IEnumerator Start()
    {
        tokenSetter.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        rankLoader.Load();
    }
}
