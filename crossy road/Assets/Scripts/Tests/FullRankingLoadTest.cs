using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullRankingLoadTest : MonoBehaviour
{
    [SerializeField] private BurntTokenSetterTest tokenSetter = null;
    [SerializeField] private UsernamesLoader usernamesLoader = null;
    [SerializeField] private JsonDataTest rankingData = null;

    private RankingData[] scoreRank;

    private void OnEnable()
    {
        usernamesLoader.OnLoaded += FillUpRanking;
    }

    private void OnDisable()
    {
        usernamesLoader.OnLoaded -= FillUpRanking;
    }

    private IEnumerator Start()
    {
        tokenSetter.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        scoreRank = GetScoreRankData();
        var listIds = RankingData.GetListIds(scoreRank);
        usernamesLoader.Load(listIds);
    }

    private RankingData[] GetScoreRankData()
    {
        var scoreRankData = JSON.Parse(rankingData.Json);
        var scoreRank = new List<RankingData>();
        for (int i = 0; i < scoreRankData.Count; i++)
        {
            if (scoreRankData[i]["objectid"] == SessionData.ConfigId)
            {
                var rankingData = new RankingData(scoreRankData[i], i);
                scoreRank.Add(rankingData);
                Debug.Log(rankingData.ToString());
            }
        }
        return scoreRank.ToArray();
    }
    private void FillUpRanking(string[] usernames)
    {
        for (int i = 0; i < scoreRank.Length; i++)
        {
            scoreRank[i].Name = usernames[i];
            Debug.Log(scoreRank[i].ToString());
        }
    }

}
