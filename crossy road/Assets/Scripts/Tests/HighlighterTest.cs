using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlighterTest : MonoBehaviour
{
    [SerializeField] private Highlighter highlighter = null;

    private void Start()
    {
        highlighter.Highlight(true);
    }
}
