using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionJsonDataTest : MonoBehaviour
{
    [SerializeField, TextArea(2, 4)] private string questionsJson = null;
    [SerializeField] private GameObject testPlayBtn = null;
    [SerializeField] private GameObject playBtn = null;

    private void Start()
    {
        playBtn.SetActive(false);
        testPlayBtn.SetActive(true);
        foreach (var item in JSON.Parse(questionsJson))
        {
            User.Questions.Add(JsonUtility.FromJson<QuestionData>(item.Value.ToString()));
        }
        Local.IsLocal = true;
    }

    
}
