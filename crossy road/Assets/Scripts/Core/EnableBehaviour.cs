using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class EnableBehaviour : MonoBehaviour
    {
        public virtual bool Enabled { get; set; } = true;
    }

}
