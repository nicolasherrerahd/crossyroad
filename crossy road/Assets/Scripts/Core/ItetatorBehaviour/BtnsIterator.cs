﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnsIterator : Iterator
{
    [SerializeField] Button btnPrevious, btnNext;

    private void Start() => SetButtonsBehaviour();

    protected override void OnEnable()
    {
        base.OnEnable();
        UpdateButtons();
    }

    private void SetButtonsBehaviour()
    {
        btnPrevious.onClick.AddListener(PreviousIterable);
        btnNext.onClick.AddListener(NextIterable);
    }

    protected override void GoToIterable(int amount)
    {
        base.GoToIterable(amount);
        UpdateButtons();
    }

    private void UpdateButtons()
    {
        btnPrevious.gameObject.SetActive(!IsFirstPosition);
        btnNext.gameObject.SetActive(!IsLastPosition);
    }
}
