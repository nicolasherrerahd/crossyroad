﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DwellEvent
{
    public UnityEvent actionEvent = null;
    public float delay = 0f;
}
