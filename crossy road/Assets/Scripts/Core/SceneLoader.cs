﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CrossyRoad
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        [SerializeField] private GameObject panelLoading = null;
        [SerializeField] private Animator fadeAnim = null;

        public int CurrentSceneIndex => SceneManager.GetActiveScene().buildIndex;

        public string CurrentSceneName => SceneManager.GetActiveScene().name;

        public void LoadFirstScene() => LoadScene(0);

        public void LoadNextScene() => LoadScene(CurrentSceneIndex + 1);

        public void LoadPreviousScene() => LoadScene(CurrentSceneIndex - 1);

        public void ReloadScene() => LoadScene(CurrentSceneIndex);

        public void LoadScene(int sceneIndex) => StartCoroutine(CallLoadScene(sceneIndex));

        public void LoadScene(string sceneName) => StartCoroutine(CallLoadScene(sceneName));

        #region Coroutines
        IEnumerator CallLoadScene(string sceneName)
        {
            if (fadeAnim != null)
                yield return FadeOut();
            if (panelLoading == null)
                SceneManager.LoadScene(sceneName);
            else
                yield return CallAsyncSceneLoad(sceneName);

        }

        IEnumerator CallLoadScene(int sceneIndex)
        {
            var scenePath = SceneUtility.GetScenePathByBuildIndex(sceneIndex);
            yield return CallLoadScene(GetSceneNameByPath(scenePath));
        }


        IEnumerator CallAsyncSceneLoad(string sceneName)
        {
            AsyncOperation asyncSceneLoad = SceneManager.LoadSceneAsync(sceneName);
            panelLoading.SetActive(true);
            while (!asyncSceneLoad.isDone)
                yield return null;
        }

        IEnumerator FadeOut()
        {
            fadeAnim.SetTrigger("fadeOut");
            yield return new WaitForEndOfFrame();
            float length = fadeAnim.GetCurrentAnimatorStateInfo(0).length;
            yield return new WaitForSeconds(length);
        }
        #endregion

        #region Static Methods
        public static string GetSceneNameByPath(string path)
        {
            var pathRoutes = path.Split('/');
            var sceneNameFormat = pathRoutes[pathRoutes.Length - 1];
            var format = ".unity";
            return sceneNameFormat.Remove(sceneNameFormat.Length - format.Length);
        }
        #endregion

        public void Quit() => Application.Quit();

    }

}
