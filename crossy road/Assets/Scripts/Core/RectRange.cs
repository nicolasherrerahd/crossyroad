using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossyRoad
{
    public class RectRange : MonoBehaviour
    {
        [field: SerializeField] public Vector2 MinAxes { get; private set; } = new Vector2(-1f, -2f);
        [field: SerializeField] public Vector2 MaxAxes { get; private set; } = new Vector2(1f, 2f);
        [SerializeField] private Color gizmoColor = Color.cyan;

        public bool HasRange => Vector2.Distance(MinAxes, MaxAxes) > 0;

        private void OnDrawGizmos()
        {
            Gizmos.color = gizmoColor;
            DrawRect();
            CheckAxesRange();
        }

        private void CheckAxesRange()
        {
            if (!HasRange)
                Gizmos.DrawSphere(MinAxes, 0.1f);
        }

        private void DrawRect()
        {
            Vector2 bottomLeft = MinAxes;
            Vector2 topLeft = new Vector2(MinAxes.x, MaxAxes.y);
            Vector2 bottomRight = new Vector2(MaxAxes.x, MinAxes.y);
            Vector2 topRight = MaxAxes;
            Gizmos.DrawLine(bottomLeft, bottomRight);
            Gizmos.DrawLine(bottomRight, topRight);
            Gizmos.DrawLine(topRight, topLeft);
            Gizmos.DrawLine(topLeft, bottomLeft);
        }

        public Vector3 GetRandomPosWithinRange(float unitsX)
        {
            float offset = MinAxes.x == MaxAxes.x ? 0f : (unitsX / 2) * 0.77f;
            return new Vector3(Random.Range(MinAxes.x + offset, MaxAxes.x - offset), Random.Range(MinAxes.y, MaxAxes.y), 0f);
        }
    }

}
