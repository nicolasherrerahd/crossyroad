window.addEventListener("message", function(evt){
    if(evt.origin !== "localhost" || evt.origin !== "clapdev.com" || evt.origin !== "clapstaging.com" || evt.origin !== "clapprod.com"){
        window.unityInstance.SendMessage('DataRetriever', 'SetUrl', evt.origin);
        window.unityInstance.SendMessage('DataRetriever', 'SetData', evt.data);
    } else {
        window.alert('No permitido');
    }
}, false);

function communicate(commObj) {
    console.log("Posting message v11" + commObj);
    window.parent.postMessage(commObj, "*");
}

function testLog()
{
    console.log("It works!");
}