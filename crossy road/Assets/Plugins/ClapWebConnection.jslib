mergeInto(LibraryManager.library, {

    AlertLog: function (succeeded, logStr) {
        if (succeeded) {
            window.alert("Check console for details.");
        } else {
            window.alert("Error retrieving data");
        }
        console.log(logStr);
    },

    Communicate: function (commObj) {
        window.communicate(Pointer_stringify(commObj));
    },

    TestLog: function (isLogging) {
        if (isLogging){
            window.testLog();
        }
    },

    IsMobile: function()
    {
        return (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent));
    }


});